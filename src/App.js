import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import AdminDashboard from './pages/AdminDashboard';

import Products from './pages/Products';
import ProductView from './components/ProductView';
import CreateProduct from './pages/CreateProduct';
import EditProduct from './pages/EditProduct';


import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';


import { useState, useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { UserProvider } from './UserContext';

import './App.css';



function App() {

  // State hook for the user state that defines here for global scoping
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing the localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  };


  // useEffect
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    });
  }, []);


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
         <AppNavBar />
          <Container>
            <Routes>
              <Route path ='/' element = {<Home/>}/>
              <Route path ='/register' element = {<Register/>}/>
              <Route path = '/login' element = {<Login/>} />
              <Route path = '/adminDashboard' element = {<AdminDashboard/>} />
              <Route path = '/logout' element = {<Logout/>} />
              <Route path = '/products' element = {<Products/>}/>
              <Route path = '/products/:id' element = {<ProductView/>} />
              <Route path = '/createProduct' element = {<CreateProduct/>} />
              <Route path = '/editProduct' element = {<EditProduct/>} />
              <Route path = '*' element = {<PageNotFound/>} />
            </Routes>
         </Container>
      </BrowserRouter>
    </UserProvider>
  );
}



export default App;
