import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
	
	const backgroundImageUrl = 'https://drive.google.com/uc?export=view&id=1FrKgYKTpCGNjkOx3rYLpqk4_PrKOQTTj';

	return (
		<Row className="app-banner-container">
			<Col xs={12} md={6} className="image-column" style={{ backgroundImage: `url(${backgroundImageUrl})` }}>
			</Col>
			<Col xs={12} md={6} className="content-column">
				<h1 className="cookie-heading">Bake me a Cookie</h1>
				<p>We're here to bake you happy!</p>
				<Button as={Link} to="/products" className="rubber-radish">
				Order Now
				</Button>
			</Col>
		</Row>
	)
}