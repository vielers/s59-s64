import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavBar() {

	const { user } = useContext(UserContext);
	
	return (
		<Navbar expand="lg" className="rubber-radish text-light fixed-top">
		    <Container fluid>
		        <Navbar.Brand as = {Link} to = '/'>The Cookie Bakery</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
		            
		           {user.id === null || user.id === undefined 
		           ? (
		                <>
		                    <Nav.Link as={NavLink} to="/register">
		                             Register
		                    </Nav.Link>
		                    <Nav.Link as={NavLink} to="/login">
		                             Login
		                    </Nav.Link>
		                    
		                </>
		            ) : (
		                <>
		                    {user.isAdmin === true && (
		                        <Nav.Link as={NavLink} to="/adminDashboard">
		                                    Admin Dashboard
		                        </Nav.Link>
		                    )}
		                        <Nav.Link as={NavLink} to="/logout">
		                                  Logout
		                        </Nav.Link>
		                </>
		            )}


		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}