import { Fragment } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminCard() {

	return (

		<Fragment>
			<Card style={{ width: '18rem' }}>
				<Card.Body>
					<Card.Title>Add New Product</Card.Title>
					
					<Button as = {Link} to = {'/createProduct'} variant = "primary">Create a Product</Button>
				</Card.Body>
			</Card>

			<Card style={{ width: '18rem' }}>
				<Card.Body>
					<Card.Title>View/Edit Products</Card.Title>
					<Button as = {Link} to = {'/editProduct'} variant = "primary">View/Edit all Products</Button>
				</Card.Body>
			</Card>

		</Fragment>

	);
}