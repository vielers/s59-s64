import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function ProductCard({productProp}) {

	// Checks to see if the data was successfully passed
	console.log('This is from ProductCard.');
	console.log(productProp);

	const{_id, name, description, price, stockQty} = productProp;

	// STATE HOOKS
	const [order, setOrder] = useState();

	const [stocks, setStocks] = useState();

	// State hook that will declare the value of the disabled property in the button
	const [isDisabled, setIsDisabled] = useState();

	const navigate = useNavigate();



	function checkout() {
		if (stocks > 1) {
			setOrder(order + 1);
			setStocks(stocks - 1);
		} else {
			setStocks(stocks - 1);
		};
	}


	useEffect(() => {
		if (stocks === 0) {
			setIsDisabled(true);
		}
	}, [stocks]);


	return (

		<Container fluid className="product-card-container">
            {/* Product Card */}
            <Row>
                <div className="product-card-wrapper">
                    <Card className="product-card-card">
                        <Card.Body className="d-flex flex-column align-items-center justify-content-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Text>Stock : {stockQty}</Card.Text>
                            <Button as={Link} to={`/products/${_id}`} variant="primary" disabled={isDisabled} onClick={checkout}>I want this cookie</Button>
                        </Card.Body>
                    </Card>
                </div>
            </Row>


		</Container>
	)

}


ProductCard.propTypes = {
	ProductProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stockQty: PropTypes.number
	})
}


/*return (
	    <div className="product-card-container">
	    	<div className="product-card-wrapper">
	    		<Card className="product-card-card">
	    		    <Card.Body className="d-flex flex-column align-items-center justify-content-center">
	    		        <Card.Title>{name}</Card.Title>
	    		        <Card.Subtitle>Description:</Card.Subtitle>
	    		        <Card.Text>{description}</Card.Text>
	    		        <Card.Subtitle>Price:</Card.Subtitle>
	    		        <Card.Text>{price}</Card.Text>
	    		        <Card.Text>Stock : {stockQty}</Card.Text>
	    		        <Button as = {Link} to = {`/products/${_id}`} variant="primary" disabled={isDisabled} onClick={checkout}>I want this cookie</Button>
	    		    </Card.Body>
	    		</Card>
	    	</div>
	    </div>
	)*/