import { Row, Col, Button, Card } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [stockQty, setStockQty] = useState('');
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);

	const {id} = useParams();
	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const handleChange = (event) => {
		const newQty = parseInt(event.target.value);
		setQuantity(newQty);
		setTotalAmount(newQty * price);
	};

	const incrementQty = () => {
		setQuantity(quantity + 1);
		setTotalAmount((quantity + 1) * price);
	};

	const decrementQty = () => {
		if (quantity > 0) {
			setQuantity(quantity - 1);
			setTotalAmount((quantity - 1) * price);
		}
	};


	useEffect(() => {
		if (user.isAdmin === true) {
			Swal2.fire({
			    title: "Admin user cannot use the order function!",
			    icon: "error",
			    text: "Please log in as a regular user to place an order."
			});
			navigate('/products');
		}
	})


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setPrice(data.price);
			setDescription(data.description);
			setStockQty(data.stockQty);
		})

	}, [])


	const checkout = (productId, productName, quantity, price, totalAmount) => {
		
		if (quantity === 0) {
		    Swal2.fire({
		        title: "Invalid quantity!",
		        icon: "error",
		        text: "Please input a valid quantity."
		    });
		    	return;
		}


		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: `${productId}`,
      			productName: `${productName}`, // Pass the product name
      			quantity: quantity, // Pass the quantity
      			price: `${price}`,
      			totalAmount: totalAmount
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log("Response from server:", data);
			if(data.success === true) {

				Swal2.fire({
					title: "We've got your order!",
					icon: "success",
					text: "Your cookie goodies will be delivered soon!"
				})
				navigate('/products');
			} else {
				Swal2.fire({
					title: "Order not submitted!",
					icon: "error",
					text: data.message // Show the error message from the server
				})
			}
		});
	}


	


	return (
		<Row className="product-card-container">
			<Col>
				<Card>
				    <Card.Body>
				      <Card.Title>{name}</Card.Title>
				      <Card.Text>{description}
				      </Card.Text>
				      <Card.Text>Price: {price}
				      </Card.Text>

				      <div className="custom-input align-items-center">
				      	<div className = "input-group d-flex align-items-center justify-content-center">
				      		<div className = "input-group-prepend">
				      			<button className = "btn btn-outline-secondary" type = "button" onClick = {decrementQty}>
				      			-
				      			</button>
				      		</div>
				      		<input 
				      			type = "number" 
				      			className = "form-control custom-input" 
				      			value = {quantity} 
				      			onChange = {handleChange}
				      			style={{ width: '20px' }}
				      		/>
				      		<div className = "input-group-append">
				      			<button className = "btn btn-outline-secondary" type = "button" onClick = {incrementQty}>
				      			+
				      			</button>
				      		</div>
				      	</div>
				      	<p className="text-center">Total Amount: {totalAmount}</p>
				      </div>

				      <Button variant="primary" onClick = {() => checkout(id, name, quantity, price, totalAmount)}>Order</Button>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

