import { Fragment, useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';

// import coursesData

export default function Products() {

	// STATE HOOKS
	const [products, setProducts] = useState();

	// useEffect to fetch the updated content of the products in the database
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		});
	}, []);

	return(
		<Fragment>
			{products}
		</Fragment>
	)
}