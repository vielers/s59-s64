import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Swal2 from 'sweetalert2';
import PropTypes from 'prop-types';
import { Container, Col, Row, Table, Button } from 'react-bootstrap';

export default function EditProduct({productProp}) {

	const navigate = useNavigate();
	const {id, name, description, price, stockQty} = useParams();
	
	// const{ name, description, price, stockQty } = productProp;

	// const [name, setName] = useState('');
	// const [description, setDescription] = useState('');
	// const [price, setPrice] = useState('');
	// const [stockQty, setStockQty] = useState('');
	
	const [products, setProducts] = useState([]);
	// const {productId} = useParams();

	const getButtonVariant = (isActive) => {
	    return isActive ? 'danger' : 'success';
	  };

	useEffect(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
	      .then(response => response.json())
	      .then(data => {
	        setProducts(data);
	      })
	      .catch((error) => {
	      	console.log('Error fetching products:', error);
	      });
	  }, []);

	
	const getAvailabilityStatus = stockQty => {
		if (stockQty > 0) {
			if (stockQty <= 5) {
				return <span style={{ color: 'orange'}}>Low Stock</span>;
			} else {
				return <span style={{ color: 'green'}}>Available</span>;
			}
		} else {
			return <span style={{ color: 'red'}}>Unavailable</span>;
		}
	};


	const handleUpdateProduct = (productId, productName, productPrice, productStockQty) => {
	  	

	  	Swal2.fire({
	    	title: 'Update Product',
	    	html: `
	      		<p>Product Name: ${productName}</p>
	      		<p>Price: ${productPrice}</p>
	      		<p>Stock Quantity: ${productStockQty}</p>
	      		<input id="newPrice" type="number" placeholder="New Price" />
	      		<input id="newStockQty" type="number" placeholder="New Stock Quantity" />
	    	`,
	    	showCancelButton: true,
	    	confirmButtonText: 'Update',
	    	preConfirm: () => {
	      		const newPrice = document.getElementById('newPrice').value;
	      		const newStockQty = document.getElementById('newStockQty').value;
	      		return { newPrice, newStockQty };
	    	},
	  	}).then((result) => {
	    	if (result.isConfirmed) {
	      	const { newPrice, newStockQty } = result.value;

	      	// Prepare the updated product data
	      	const updatedProductData = {
	        	price: parseFloat(newPrice),
	        	stockQty: parseInt(newStockQty)
	      	};

	      	// Log the JSON string to check the format
	      	console.log('Updated Product Data:', JSON.stringify(updatedProductData));

	      	// Send the update request to the server
	      	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}?_cache=${Date.now()}`, {
	        	method: 'PUT',
	        	headers: {
	          		'Content-Type': 'application/json',
	          		'Authorization': `Bearer ${localStorage.getItem('token')}`
	        	},
	        	body: JSON.stringify(updatedProductData)
	      	})
	        .then((response) => {
	        	if (!response.ok) {
	        		throw new Error('Network response was not OK');
	        	}
	        	return response.json();
	        })
	        .then((data) => {
	        	console.log(data);
	          	if (data.success === false) {
	            	
	            	// Check if the response has a custom error message
	            	const errorMessage = data.message;
	            	if (errorMessage && errorMessage.trim() !== '') {
	            	    Swal2.fire('Error updating product', errorMessage, 'error');
	            	} else {
	            	    Swal2.fire('Error updating product', 'Failed to update the product.', 'error');
	            	}

	            	

	          	} else {
	            	
	          		// Fetch the updated products from the server
	          		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
	          		.then(response => response.json())
	          		.then(updatedProductsData => {
	          		    // Update the state with the latest data
	          		    setProducts(updatedProductsData);
	          		    Swal2.fire('Product updated!', 'The product has been updated successfully.', 'success');
	          		    navigate('/editProduct');
	          		})
	          		.catch((error) => {
	          		    console.log('Error fetching updated products:', error);
	          		    Swal2.fire('Product updated!', 'The product has been updated successfully.', 'success');
	          		    navigate('/editProduct');
	          		});

	          	}
	        })
	        .catch((error) => {
	          	console.log('Error updating product:', error);
	          	Swal2.fire('Error updating product', 'An error occurred while updating the product.', 'error');
	        });
	    	}
	  	});
	};

	

	const handleToggleProductStatus = (productId, productName, isActive) => {
	    const action = isActive ? 'archive' : 'activate';

	    Swal2.fire({
	      title: `Confirm ${action.charAt(0).toUpperCase() + action.slice(1)} Product`,
	      text: `Are you sure you want to ${action} the product "${productName}"?`,
	      showCancelButton: true,
	      confirmButtonText: action.charAt(0).toUpperCase() + action.slice(1),
	      confirmButtonColor: isActive ? 'red' : 'green',
	    }).then((result) => {
	      if (result.isConfirmed) {
	        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${action}`, {
	          method: 'PUT',
	          headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	          }
	        })
	          // .then((response) => response.json())
	          .then((response) => {
	          	console.log('Response from server:', response);
	          	if (response.ok) {
	          		return response.json();
	          	} else {
	          		throw new Error('Network response was not OK');
	          	}
	          })
	          .then((data) => {
	          	console.log(data);
	            if (data) {
	              const actionVerb = isActive ? 'archived' : 'activated';
	              Swal2.fire(`Product is now ${actionVerb}!`, `The product has been ${actionVerb} successfully.`, 'success');
	              navigate('/editProduct');
	            } else {
	              Swal2.fire('Error updating product', 'Failed to update the product.', 'error');
	            }
	          })
	          .catch((error) => {
	            console.log('Error updating product:', error);
	            Swal2.fire('Error updating product', 'An error occurred while updating the product.', 'error');
	          });
	      }
	    });
	  };





	return (
	    <Container className="edit-product-table-container">
	    	<Row>
	    		<Col>
	    			<h1 className="edit-product-header">View/Edit All Products</h1>
	    			<Table striped bordered hover className="edit-product-table">
	    			  <thead>
	    			    <tr>
	    			      <th>No.</th>
	    			      <th>Name</th>
	    			      <th>Description</th>
	    			      <th>Price</th>
	    			      <th>Stock</th>
	    			      <th>Availability</th>
	    			      <th>Actions</th>
	    			    </tr>
	    			  </thead>
	    			  <tbody>
	    			    {products.map((product, index) => (
	    			      <tr key={product._id}>
	    			        <td>{index + 1}</td>
	    			        <td>{product.name}</td>
	    			        <td>{product.description}</td>
	    			        <td>{product.price}</td>
	    			        <td>{product.stockQty}</td>
	    			        <td>{getAvailabilityStatus(product.stockQty)}</td>
	    			        <td className="text-center">
	    			          <button 
	    			          	className="variant-primary"
	    			            onClick={() => handleUpdateProduct(product._id, product.name, product.price, product.stockQty)}
	    			          >
	    			            Update
	    			          </button>
	    			          <button 
	    			          	className={product.isActive ? 'variant-danger' : 'variant-success'}
                    			onClick={() =>
                      				handleToggleProductStatus(
                        				product._id,
                        				product.name,
                        				product.isActive
                      				)}
                  				>
                    				{product.isActive ? 'Disable' : 'Enable'}
	    			          </button>
	    			        </td>
	    			      </tr>
	    			    ))}
	    			  </tbody>
	    			</Table>
	    		</Col>
	    	</Row>
	    </Container>
	);
}


EditProduct.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stockQty: PropTypes.number,
		isActive: PropTypes.bool,
	}),
};

