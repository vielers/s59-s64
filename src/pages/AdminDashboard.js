import { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AdminCard from '../components/AdminCard';
import UserContext from '../UserContext';


export default function AdminDashboard() {

  	return (
      	<div className="admin-dashboard-container">
        	<h1 className="admin-dashboard-header">Welcome Admin!</h1>
        	<div className="admin-card-wrapper">
          		<Card className="admin-dashboard-card" style={{ backgroundColor: '#ffb3bc' }}>
            		<Card.Body className="d-flex flex-column align-items-center justify-content-center">
              			<Card.Title>Add New Product</Card.Title>
              			<Button as={Link} to={'/createProduct'} variant="outline-dark" className="admin-card-button">
                		Create a Product
              			</Button>
            		</Card.Body>
          		</Card>
          		<Card className="admin-dashboard-card" style={{backgroundColor: '#a6fdfc'}}>
            		<Card.Body className="d-flex flex-column align-items-center justify-content-center">
              			<Card.Title>View/Edit All Products</Card.Title>
              			<Button as={Link} to={'/editProduct'} variant="outline-dark" className="admin-card-button">
                		View/Edit Products
              			</Button>
            		</Card.Body>
          		</Card>
        	</div>
      	</div>
    );
}