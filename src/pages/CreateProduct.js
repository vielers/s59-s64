import { Button, Form, Row, Col } from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import Swal2 from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import AdminDashboard from './AdminDashboard';


export default function CreateProduct() {

	// STATE HOOKS
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stockQty, setStockQty] = useState('');

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);


	// useEffects
	// useEffect that will ENABLE the SIGN UP button if all the set conditions below are met. Hence, DISABLED if the set conditions are NOT met.

	useEffect(() => {
		if (
			name !== '' &&
			description !== '' &&
			price !== '' &&
			stockQty !== ''
		) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	
	}, [name, description, price, stockQty]);


/*	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(response => response.json())
		.then(data => {
			console.log('API Response:', data); //Log the response data
			if (data && data.length > 0){
				const existingProduct = data.find(product => product.name === name); 
				console.log('Existing Product:', existingProduct); //Log the existingProduct
				setName(existingProduct.name);
				setPrice(existingProduct.price);
				setDescription(existingProduct.description);
				setStockQty(existingProduct.stockQty);
			}
		})
		.catch(error => {
		    console.error('Error fetching products:', error);
		});
	}, [name]);*/


// 	useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
  //     .then(response => response.json())
  //     .then(data => {
  //       console.log('API Response:', data);
  //     })
  //     .catch(error => {
  //       console.error('Error fetching products:', error);
  //     });
  // }, []);
	

	// Function to simulate creating a product
	function addProduct(event) {

		event.preventDefault();

		// Fetching the data to check whether the product is already registered prior proceeding to product creation
		fetch(`${process.env.REACT_APP_API_URL}/products/checkProduct`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data) {
				Swal2.fire({
					title: 'Duplicate product found!',
					icon: 'info',
					text: 'Product already registered.'
				});
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${localStorage.getItem('token')}`,
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						stockQty: stockQty
					})
				})
				.then(response => response.json())
				.then((data) => {
					console.log('Response from server:', data); //Log the response data from the server
					if (data.name) {
						Swal2.fire({
							title: 'Successful creating a new product!',
							icon: 'success',
							text: 'This product is now added.'
						})
						navigate('/adminDashboard');
					} else {
						Swal2.fire({
							title: 'Unsuccessful creating a product!',
							icon: 'error',
						});
					}
					
				})
				.catch(error => {
					// Handle error in the fetch or API call
					console.error("Error adding product:", error);
				});
			}
		})
		.catch(error => {
			// Handle error in the fetch or API call
			console.error("Error fetching products:", error);
		});
	}

	return (
		user && user.isAdmin
		?
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Create a new product</h1>
					<Form onSubmit = {event => addProduct(event)}>

						<Form.Group className="mb-3" controlId="name">
							<Form.Label>Product Name</Form.Label>
							<Form.Control 
								type = "text"
								placeholder = "Enter product name"
								value = {name}
								required
								onChange = {event => setName(event.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="description">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type = "text"
								placeholder = "Enter product description"
								value = {description}
								required
								onChange = {event => setDescription(event.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="price">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type = "number"
								placeholder = "Enter product price"
								value = {price}
								required
								onChange = {event => setPrice(parseFloat(event.target.value))}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="stockQty">
							<Form.Label>Stock</Form.Label>
							<Form.Control 
								type = "number"
								placeholder = "Enter available stocks"
								value = {stockQty}
								required
								onChange = {event => setStockQty(parseInt(event.target.value))}
							/>
						</Form.Group>

						<Button variant = "primary" type = "submit" disabled = {isDisabled}>
							Add Product
						</Button>

					</Form>
				</Col>
			</Row>
		:
			<AdminDashboard />
	)
}