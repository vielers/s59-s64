import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

	const navigate = useNavigate();

	// STATE HOOKS
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// The SUBMIT/LOGIN button is enabled if all the input fields are filled.
	const [isActive, setIsActive] = useState(true);

	// This allows us to consume the UserContext object and its properties to use for user validation
	const { user, setUser } = useContext(UserContext);


	// useEffect that will ENABLE the SIGN UP button if all the set conditions below are met. Hence, DISABLED if the set conditions are NOT met.
	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(false);
		} else {
			setIsActive(true);
		}
	}, [email, password]);


	// useEffect for admin user to navigate to adminDashboard after login
	useEffect(() => {
	  if (user && user.isAdmin) {
	    navigate('/adminDashboard');
	  }
	}, [user]);



	// Retrieving user details
	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		});
	}




	function authenticate(event) {

		// Prevents page reloading/redirection via form submission
		event.preventDefault();

		// Fetching the data
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {

			// if statement to check whether the login is successful
			if(data === false) {
				Swal2.fire({
					title: 'Login unsuccessful.',
					icon: 'error',
					text: 'Check your login credentials and try again.'
				});
			} else {
				localStorage.setItem('token', data.access);

				retrieveUserDetails(data.access);

				Swal2.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: "IT's COOKIE TIME!"
				});

				if (data.isAdmin) {
					navigate('/adminDashboard');

				} else {
					navigate('/');
				}
			}
		});
	}


	
	return (
		user.id === null || user.id === undefined
		? 
		(
			<Row className="mt-20 login-container">
			    <Col className = 'col-6 mx-auto'>
			        <h1 className = 'text-center mt-2' style={{ fontFamily: 'Pacifico' }}>Login</h1>
			        <Form onSubmit={(e) => authenticate(e)} className="border p-4">
			            <Form.Group controlId="userEmail">
			                <Form.Label>Email address:</Form.Label>
			                <Form.Control 
			                    type="email" 
			                    placeholder="Enter email"
			                    value={email}
			                    onChange={(e) => setEmail(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="password">
			                <Form.Label>Password:</Form.Label>
			                <Form.Control 
			                    type="password" 
			                    placeholder="Password"
			                    value={password}
			                    onChange={(e) => setPassword(e.target.value)}
			                    required
			                />
			            </Form.Group>
			            
			            <Button variant="primary" type="submit" id="submitBtn" disabled = {isActive} className ='mt-3'>
			                    Login
			                </Button>

			        </Form>
			        
			        <div className="text-center mt-3">
			            Don't have an account yet? Click{' '}
			         	<Link to="/register">here</Link> to register.
			        </div>
			    </Col>
			</Row>

		) : user.isAdmin ? (
			// Navigating to admin dashboard if the user is Admin
			<Navigate to = '/adminDashboard' />
		
		) : (
			//navigating to PageNotFound once binago yung endpoint to /login
			<Navigate to ='/*' />
		)	    
	);
}