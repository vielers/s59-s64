import { Button, Form, Row, Col } from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import Swal2 from 'sweetalert2';
import { useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Login from './Login';


// Regular expression for email validation
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export default function Register() {

	// STATE HOOKS
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	// To check if the email is valid
	const [isValidEmail, setIsValidEmail] = useState(false);

	// This is for the enabling the SIGNUP/SUBMIT button when all the input fields are FILLED
	const [isDisabled, setIsDisabled] = useState(true);

	// This is the state that will declare whether the password1 and password2 are equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true);


	// useEffects
	// useEffect for checking if the email is valid
	useEffect(() => {
		
		// Check if the email matches the regex pattern
		setIsValidEmail(emailRegex.test(email));
	}, [email]);


	// useEffect to validate whether the password1 is equal or matched to password2
	useEffect(() => {
		if (password1 !== password2) {
			setIsPasswordMatch(false);
		} else {
			setIsPasswordMatch(true);
		}
	}, [password1, password2]);


	// useEffect that will ENABLE the SIGN UP button if all the set conditions below are met. Hence, DISABLED if the set conditions are NOT met.
	useEffect(() => {

		if (
			firstName !== '' &&
			lastName !== '' &&
			email !== '' &&
			password1 !== '' &&
			password2 !== '' &&
			password1 === password2 &&
			emailRegex.test(email) // Use the test method to check email against the regex pattern
		) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [firstName, lastName, email, password1, password2]);


	// Function to simulate the user registration
	function registerUser(event) {

		// Prevent page reloading
		event.preventDefault();

		// Fetching the data
		// This will check first if the user's email is already registered.
		// Registration will not proceed if the email already exists.
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then((data) => {
			if (data) {
				Swal2.fire({
					title: 'Duplicate email found!',
					icon: 'info',
					text: 'Email already exists.'
				});
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1
					})
				})
				.then(response => response.json())
				.then((data) => {
					if (data.email) {
						Swal2.fire({
							title: 'Registration unsuccessful!',
							icon: 'error',
							text: 'Check your registration details and register again.'
						});
					} else {
						Swal2.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: "You are now registered. IT's COOKIE TIME!"
						})
						navigate('/login');
					}
				});
			}
		});
	}



	return(
		user.id === null || user.id === undefined
		?
			<div className="register-container">
				<Row className="justify-content-center align-items-center h-100">
					<Col md={6}>
						<h1 className = "text-center mb-4" style={{ fontFamily: 'Pacifico' }}>Register</h1>
						<Form onSubmit = {event => registerUser(event)} className="register-form">

							<Form.Group className="mb-3" controlId="firstName">
						        <Form.Label>First Name</Form.Label>
						        <Form.Control 
						        	type="text" 
						        	placeholder="Enter First Name"
						        	value = {firstName}
						        	required
						        	onChange = {event => setFirstName(event.target.value)}
						        />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="lastName">
						        <Form.Label>Last Name</Form.Label>
						        <Form.Control 
						        	type="text" 
						        	placeholder="Enter Last Name"
						        	value = {lastName}
						        	required
						        	onChange = {event => setLastName(event.target.value)}
						        />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="userEmail">
						        <Form.Label>Email address</Form.Label>
						        <Form.Control 
						        	type="email" 
						        	placeholder="Enter email"
						        	value = {email}
						        	required
						        	onChange = {event => setEmail(event.target.value)}
						        />
						        <Form.Text className="text-danger">
						          {isValidEmail ? <p>Email is valid</p> : <p>Email is not valid</p>}
						        </Form.Text>
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="password1">
						        <Form.Label>Password</Form.Label>
						        <Form.Control 
						        	type="password" 
						        	placeholder="Password"
						        	value={password1}
						        	required
						        	onChange={event => setPassword1(event.target.value)}
						        />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="password2">
						        <Form.Label>Confirm Password</Form.Label>
						        <Form.Control 
						        	type="password" 
						        	placeholder="Retype your nominated password"
						        	value={password2}
						        	required
						        	onChange={event => setPassword2(event.target.value)} 
						        />
						    </Form.Group>

						    <Form.Text className="text-danger" hidden = {isPasswordMatch}>
						    	The passwords does not match!
						    </Form.Text>

						      
						    <Button variant="primary" type="submit" disabled = {isDisabled}>
						    	Sign up
						    </Button>
						</Form>

						<div className="text-center mt-3">
				            Already have an account? Click{' '}
				         	<Link to="/login">here</Link> to login.
				        </div>

					</Col>	
				</Row>
			</div>
			
		:
			<Login />
	)	
}